<?php
    if($list_menu){
        $id_layanan         = $list_menu["id_layanan"];
        $id_jenis           = $list_menu["id_jenis"];
        $id_kategori        = $list_menu["id_kategori"];
        $id_sub_kategori    = $list_menu["id_sub_kategori"];
    }
?>

            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h3>Masukkan Kelengkapan Data Antrean</h3>
                            </div>
                            <div class="card-body">
                                <br>
                                <div class="form-material">
                                    <div class="form-group">
                                        <label>Nomor Induk perijinan</label>
                                        <input type="text" id="nik" class="form-control form-control-line" placeholder="3573011XXXXXXXXX" required="">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label>Nama Sesuai KTP</label>
                                        <input type="text" id="nama" class="form-control form-control-line" placeholder="Rono Dikromo" required="">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label>Tanggal Pendaftaran Antrean</label>
                                        <input type="text" id="date" class="form-control floating-label" placeholder="Date" required="">
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-lg-12 text-center">
                                    <button type="button" class="btn btn-rounded btn-info" id="add_antrean">
                                        <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;Daftarkan Antrean
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn btn-sm pull-right m-l-10"><a href="<?php echo base_url()?>beranda/layanan/" class="previous"><font size="3">&laquo; Beranda</font></a></button>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script type="text/javascript">
  var userip;
  var ip_lan;
</script>

<script type="text/javascript" src="https://l2.io/ip.js?var=userip"></script>

<script type="text/javascript">

    $(document).ready(function(){
        speak_bro();
    });

    function speak_bro(){
        try{
            // NOTE: window.RTCPeerConnection is "not a constructor" in FF22/23
            var RTCPeerConnection = /*window.RTCPeerConnection ||*/ window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
         
            if (RTCPeerConnection) (function () {
                var rtc = new RTCPeerConnection({iceServers:[]});
                if (1 || window.mozRTCPeerConnection) {      // FF [and now Chrome!] needs a channel/stream to proceed
                    rtc.createDataChannel('', {reliable:false});
                };
                
                rtc.onicecandidate = function (evt) {
                    // convert the candidate to SDP so we can run it through our general parser
                    // see https://twitter.com/lancestout/status/525796175425720320 for details
                    if (evt.candidate) grepSDP("a="+evt.candidate.candidate);
                };
                
                rtc.createOffer(function (offerDesc) {
                    grepSDP(offerDesc.sdp);
                    rtc.setLocalDescription(offerDesc);
                }, function (e) { console.warn("offer failed", e); });
                
                
                var addrs = Object.create(null);
                addrs["0.0.0.0"] = false;
                function updateDisplay(newAddr) {
                    if (newAddr in addrs) return;
                    else addrs[newAddr] = true;
                    var displayAddrs = Object.keys(addrs).filter(function (k) { return addrs[k]; });
                    console.log(displayAddrs[0]);

                    ip_lan = displayAddrs[0];
                    // document.getElementById('localIP').value = displayAddrs.join(" or perhaps ") || "n/a";
                }
                
                function grepSDP(sdp) {
                    var hosts = [];
                    sdp.split('\r\n').forEach(function (line) { // c.f. http://tools.ietf.org/html/rfc4566#page-39
                        if (~line.indexOf("a=candidate")) {     // http://tools.ietf.org/html/rfc4566#section-5.13
                            var parts = line.split(' '),        // http://tools.ietf.org/html/rfc5245#section-15.1
                                addr = parts[4],
                                type = parts[7];
                            if (type === 'host') updateDisplay(addr);
                        } else if (~line.indexOf("c=")) {       // http://tools.ietf.org/html/rfc4566#section-5.7
                            var parts = line.split(' '),
                                addr = parts[2];
                            updateDisplay(addr);
                        }
                    });
                }
            })(); else {
                
            }
        }catch(err){
            console.log(err);
        }
    }

    $("#add_antrean").click(function(){
        var data_main =  new FormData();
        data_main.append('nik', $('#nik').val());
        data_main.append('waktu', $('#date').val());
        data_main.append('id_layanan', "<?php print_r($id_layanan);?>");
        data_main.append('id_jenis', "<?php print_r($id_jenis);?>");
        data_main.append('id_kategori', "<?php print_r($id_kategori);?>");
        data_main.append('id_sub_kategori', "<?php print_r($id_sub_kategori);?>");
        data_main.append('nama', $('#nama').val());

        data_main.append('ip_lan', ip_lan);
        data_main.append('ip_public', userip);

        console.log(ip_lan);
        console.log(userip);
                    
        $.ajax({
            url: "<?php print_r(base_url()."beranda/perijinan/sendiden")?>", // point to server-side PHP script 
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                get_ticket(res);
                // console.log(res);
            }
        });

    });


    function get_ticket(res){
        var data_main = JSON.parse(res);
        
        if(data_main.msg_main.status){
            var data_detail_srv1 = data_main.msg_detail;
            
            console.log(data_main);

            var data_detail_srv2 = data_detail_srv1.item_result.msg_detail;
                var base_data_print = data_detail_srv2.data_response;
            var data_main_srv2 = data_detail_srv1.item_result.msg_main;

            console.log(data_main_srv2);
            if(data_main_srv2.status){
                console.log(data_main);
                // console.log("ok");
                var send_ip         = base_data_print.set_ip.ip_lan;
                var send_ip_public  = base_data_print.set_ip.ip_public;
                
                var send_nik        = base_data_print.data_identity.nik;
                var send_nama       = base_data_print.data_identity.nama;

                var send_layanan    = base_data_print.data_layanan.nama_page;
                var send_alamat     = base_data_print.data_layanan.alamat;
                
                var send_jenis      = base_data_print.data_jenis.nama_jenis;
                var send_kategori   = base_data_print.data_kategori.nama_kategori;
                var send_sub_kategori   = base_data_print.data_sub_kategori.nama_sub_kategori;
                
                var send_date_add   = base_data_print.data_identity.time_add;
                var send_date_book  = base_data_print.data_identity.time_book;

                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", "http://"+send_ip+":8080/print/example/interface/print_perijinan.php");
                form.setAttribute("target", "_blank");
                console.log("http://"+send_ip+":8080/print/example/interface/windows-usb.php");
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "nik");
                hiddenField.setAttribute("value", send_nik);

                var hiddenField1 = document.createElement("input");
                hiddenField1.setAttribute("type", "hidden");
                hiddenField1.setAttribute("name", "nama");
                hiddenField1.setAttribute("value", send_nama);

                var hiddenField2 = document.createElement("input");
                hiddenField2.setAttribute("type", "hidden");
                hiddenField2.setAttribute("name", "waktu_booking");
                hiddenField2.setAttribute("value", send_date_book);

                var hiddenField3 = document.createElement("input");
                hiddenField3.setAttribute("type", "hidden");
                hiddenField3.setAttribute("name", "waktu_daftar");
                hiddenField3.setAttribute("value", send_date_add);

                var hiddenField4 = document.createElement("input");
                hiddenField4.setAttribute("type", "hidden");
                hiddenField4.setAttribute("name", "layanan");
                hiddenField4.setAttribute("value", send_layanan);

                var hiddenField5 = document.createElement("input");
                hiddenField5.setAttribute("type", "hidden");
                hiddenField5.setAttribute("name", "alamat");
                hiddenField5.setAttribute("value", send_alamat);

                var hiddenField6 = document.createElement("input");
                hiddenField6.setAttribute("type", "hidden");
                hiddenField6.setAttribute("name", "jenis");
                hiddenField6.setAttribute("value", send_jenis);

                var hiddenField7 = document.createElement("input");
                hiddenField7.setAttribute("type", "hidden");
                hiddenField7.setAttribute("name", "kategori");
                hiddenField7.setAttribute("value", send_kategori);

                var hiddenField8 = document.createElement("input");
                hiddenField8.setAttribute("type", "hidden");
                hiddenField8.setAttribute("name", "sub_kategori");
                hiddenField8.setAttribute("value", send_sub_kategori);

                form.appendChild(hiddenField);
                form.appendChild(hiddenField1);
                form.appendChild(hiddenField2);
                form.appendChild(hiddenField3);
                form.appendChild(hiddenField4);
                form.appendChild(hiddenField5);
                form.appendChild(hiddenField6);
                form.appendChild(hiddenField7);
                form.appendChild(hiddenField8);

                document.body.appendChild(form);
                form.submit();

            }
        }
    }

</script>