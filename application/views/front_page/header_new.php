<!DOCTYPE html>
<!-- Template by Quackit.com -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Apel Mas</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>assets/template/template2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS: You can use this stylesheet to override any Bootstrap styles and/or apply your own styles -->
    <link href="<?php echo base_url()?>assets/template/template2/css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <table border="0" width="1200px">
                <tr>
                    <th width="250px"><img src="<?php echo base_url()?>assets/template/template1/img/logo.png" alt="" class="img-responsive logo"></a>
                    </th>
                    <th width="700px"></th>
                    <th width="250px"><img src="<?php echo base_url()?>assets/template/template1/img/malangl.png" alt="" class="img-responsive logo"></a>
                    </th>
                </tr>
            </table>
        </div>
        <!-- /.container -->
    </nav>

    <!-- Content -->

    <div class="container">

        <!-- iklan buka -->
        <div class="jumbotron feature">
            <br>

            <div class="container">

                <div id="feature-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#feature-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#feature-carousel" data-slide-to="1"></li>
                        <li data-target="#feature-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="#">
                        <img class="img-responsive" src="<?php echo base_url()?>assets/template/template1/img/event1.jpg" alt="">
                    </a>

                        </div>
                        <div class="item">
                            <a href="#">
                        <img class="img-responsive" src="<?php echo base_url()?>assets/template/template1/img/event2.jpg" alt="">
                    </a>

                        </div>
                        <div class="item">
                            <a href="#">
                        <img class="img-responsive" src="<?php echo base_url()?>assets/template/template1/img/event3.jpg" alt="">
                    </a>

                        </div>
                    </div>
                    <a class="left carousel-control" href="<?php echo base_url()?>assets/template/template2/#feature-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="<?php echo base_url()?>assets/template/template2/#feature-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <!-- iklan tutup -->

                <style>
                    body {
                        background-image: url("<?php echo base_url()?>assets/template/template1/img/balaikota.jpg");
                        background-color: #58566d;
                        background-size: cover;
                        /* Tambahkan baris berikut */
                        background-attachment: fixed;
                    }
                </style>

                <br>
                <br>
                <div class="col-md-12 ">
                    <div class="about-details">