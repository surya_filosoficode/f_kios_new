<!DOCTYPE html>
<!-- Template by Quackit.com -->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Apel Mas</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>assets/template/template2/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS: You can use this stylesheet to override any Bootstrap styles and/or apply your own styles -->
    <link href="<?php echo base_url()?>assets/template/template2/css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <table border="0" width="1200px">
                <tr>
                    <th width="250px"><img src="<?php echo base_url()?>assets/template/template1/img/logo.png" alt="" class="img-responsive logo"></a>
                    </th>
                    <th width="700px"></th>
                    <th width="250px"><img src="<?php echo base_url()?>assets/template/template1/img/malangl.png" alt="" class="img-responsive logo"></a>
                    </th>
                </tr>
            </table>
        </div>
        <!-- /.container -->
    </nav>

    <!-- Content -->

    <div class="container">

        <!-- iklan buka -->
        <div class="jumbotron feature">
            <br>
            <div class="col-xs-3 right">

                <?php

                    date_default_timezone_set('Asia/Jakarta');
                    echo date('l, d-m-Y'); echo""; 
                    echo "&nbsp" . date("h:i:sa");
                ?>
            </div>

            <div class="container">

                <div id="feature-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#feature-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#feature-carousel" data-slide-to="1"></li>
                        <li data-target="#feature-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="#">
                        <img class="img-responsive" src="<?php echo base_url()?>assets/template/template1/img/event1.jpg" alt="">
                    </a>

                        </div>
                        <div class="item">
                            <a href="#">
                        <img class="img-responsive" src="<?php echo base_url()?>assets/template/template1/img/event2.jpg" alt="">
                    </a>

                        </div>
                        <div class="item">
                            <a href="#">
                        <img class="img-responsive" src="<?php echo base_url()?>assets/template/template1/img/event3.jpg" alt="">
                    </a>

                        </div>
                    </div>
                    <a class="left carousel-control" href="<?php echo base_url()?>assets/template/template2/#feature-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="<?php echo base_url()?>assets/template/template2/#feature-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <!-- iklan tutup -->

                <style>
                    body {
                        background-image: url("<?php echo base_url()?>assets/template/template1/img/balaikota.jpg");
                        background-color: #58566d;
                        background-size: cover;
                        /* Tambahkan baris berikut */
                        background-attachment: fixed;
                    }
                </style>

                <br>
                <br>
                <div class="col-md-12 ">
                    <div class="about-details">

                        <div class="container-fluid">
                            <!-- ============================================================== -->
                            <!-- Start Page Content -->
                            <!-- ============================================================== -->
                            <!-- Row -->
                            <div class="row">
                                <?php

                                    if($list_menu){
                                        if($list_menu->msg_main->status){
                                            $detail_data = $list_menu->msg_detail;

                                                $item_layanan = $detail_data->item;
                                                $base_url = $detail_data->url_core;

                                            foreach ($item_layanan as $r_item_layanan => $v_item_layanan) {
                                                $kategori   = $v_item_layanan->kategori;
                                                    $title_kategori = $kategori->nama_kategori;

                                ?>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-xs-11">
                                                <div class="card card-info text-left">
                                                    <div class="card-header">
                                                        <h3 class="m-b-0 text-white"><i class = "mdi mdi-subdirectory-arrow-right" ></i><font color="#255892">&nbsp;<?=$title_kategori?></font></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php

                                    if(isset($v_item_layanan->list_menu)){
                                        $list_menu  = $v_item_layanan->list_menu;

                                        foreach ($list_menu as $r_list_menu => $v_list_menu) {
                                            $id_page = $v_list_menu->id_page;
                                            $nama_page = $v_list_menu->nama_page;
                                            $foto_page = $v_list_menu->foto_page;
                                            $next_page = $v_list_menu->next_page;                            
                                ?>
                                        <!-- <style type="text/css">
              body {
                color: #fff;
                background-color: #38559f;
                /*background: url('img/balaikota.jpg');*/
                background-size: cover;     /* Tambahkan baris berikut */
                background-attachment: fixed;
}
          </style> -->

                                        <div class="col-md-3">
                                            <div class="card text-center">
                                                <button style="border: none; background: none;" onclick="next_page('<?php print_r($id_page);?>', '<?php print_r($next_page);?>')">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <img src="<?php print_r($base_url.$foto_page);?>" alt="homepage" style="width: 100px; height: 100px;" />
                                                            </div>
                                                            <div class="col-lg-12">
                                                                &nbsp;
                                                            </div>
                                                            <div class="col-lg-12" style="height: 40px;">
                                                                <h4 class="card-title"><?php print_r($nama_page);?></h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>

                                        <?php
                                        }
                                    }
                                }
                            }
                        }

                    ?>
                            </div>

                            <!-- ============================================================== -->
                            <!-- End PAge Content -->
                            <!-- ============================================================== -->
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    </div>

    <!-- footer starts here -->
    <footer class="footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 footer-para">
                    <p>&copy; <a href="https://malangkota.go.id/tag/ncc/">NCC-SQUAD</a> All right reserved</p>
                </div>

                <div class="col-xs-6 text-right">
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-skype"></i></a>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/template/template2/js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/template/template2/js/bootstrap.min.js"></script>

    <!-- IE10 viewport bug workaround -->
    <script src="<?php echo base_url()?>assets/template/template2/js/ie10-viewport-bug-workaround.js"></script>

    <!-- Placeholder Images -->
    <script src="<?php echo base_url()?>assets/template/template2/js/holder.min.js"></script>

    <script src="<?php echo base_url()?>assets/template/template1/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/gmaps.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/smoothscroll.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/custom.js"></script>

    <script type="text/javascript">
        function next_page(param, url) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "<?php print_r(base_url())?>" + url);

            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "id_layanan");
            hiddenField.setAttribute("value", param);

            form.appendChild(hiddenField);

            document.body.appendChild(form);
            form.submit();
        }
    </script>

</body>

</html>