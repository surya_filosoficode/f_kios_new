
                    </div>
                </div>
            </div>

        </div>

    </div>

    </div>

    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><a href="<?php echo base_url()?>beranda/layanan/" class="previous"><font size="3">&laquo; Beranda</font></a></button>

    <!-- footer starts here -->
    <footer class="footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 footer-para">
                    <p>&copy; <a href="https://malangkota.go.id/tag/ncc/">NCC-SQUAD</a> All right reserved</p>
                </div>

                <div class="col-xs-6 text-right">
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-skype"></i></a>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/template/template2/js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/template/template2/js/bootstrap.min.js"></script>

    <!-- IE10 viewport bug workaround -->
    <script src="<?php echo base_url()?>assets/template/template2/js/ie10-viewport-bug-workaround.js"></script>

    <!-- Placeholder Images -->
    <script src="<?php echo base_url()?>assets/template/template2/js/holder.min.js"></script>

    <script src="<?php echo base_url()?>assets/template/template1/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/gmaps.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/smoothscroll.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/template/template1/js/custom.js"></script>
</body>

</html>