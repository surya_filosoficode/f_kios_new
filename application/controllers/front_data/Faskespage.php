<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Faskespage extends CI_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->library("base_url_serv");

	}

#=================================================================================================#
#----------------Halaman Utama Faskes-----------------#
#=================================================================================================#
	public function faskes_main($param){
		$data["page"] = "faskes_home";

		$url = $this->base_url_serv->get_base_url()."get/api/faskes/kesehatan/json/".$param;
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["list_menu"] 	= json_decode($result);
		$data["core_url"] 	= $this->base_url_serv->get_base_url();

		// print_r("<pre>");
		// print_r($data);
		$this->load->view("faskes/index_faskes", $data);
	
	}

#=================================================================================================#
#----------------Halaman Faskes Tiap Kecamatan-----------------#
#=================================================================================================#
     public function Faskes_v_kecamatan(){
	


		$this->load->view("faskes/Faskes_tampil_skl_kecamatan");
	
	}

#=================================================================================================#
#----------------Halaman Detail-----------------#
#=================================================================================================#

	public function faskes_detail($faskes_jenis, $param){
		$data["page"] = "faskes_detail";

		$url = $this->base_url_serv->get_base_url()."get/api/faskes/kesehatan/json/".$faskes_jenis;
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["list_menu"] 	= json_decode($result);


		$url = $this->base_url_serv->get_base_url()."get/api/faskes/kesehatan/detail/".$param;
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["detail_info"] 	= json_decode($result);
		$data["core_url"] 	= $this->base_url_serv->get_base_url();

		// print_r("<pre>");
		// print_r(json_encode($data));
		$this->load->view("faskes/index_faskes", $data);
	
	}
#=================================================================================================#
#-------------------------------------------Homepage_index----------------------------------------#
#=================================================================================================#

}
?>